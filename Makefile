about:
	@echo "General maintaining"

local-ci:
	gitlab-runner exec shell build:elastic_fusion\
			--docker-pull-policy=never
	gitlab-runner exec shell build:dynamic_fusion\
			--docker-pull-policy=never
	gitlab-runner exec shell build:kintinuous\
			--docker-pull-policy=never
	gitlab-runner exec shell build:infinitam\
			--docker-pull-policy=never
