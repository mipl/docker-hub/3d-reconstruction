# 3D Reconstruction Dockerfiles

Dockerfiles for executing open state of the art 3D reconstruction
algorithms. [Dockerhub](https://hub.docker.com/r/otaviog/3d-recons/).

Requirements:

* [nvidia-docker](https://github.com/NVIDIA/nvidia-docker)
* GNU Make

Original repositories:

* https://github.com/mihaibujanca/dynamicfusion
* https://github.com/mp3guy/Kintinuous
* https://github.com/victorprad/InfiniTAM
* https://github.com/mp3guy/ElasticFusion

Project Backlog:

* https://github.com/raulmur/ORB_SLAM2

## Device Access

Most of the projects support USB depth sensors: ASUS Xption,
RealSense and Kinect. To use one, export the environment variable
`SENSOR_DEV` to one of the following `/dev` ports:

* Kinect: `/dev/video0`, `/dev/video1`, ...
* ASUS Xption: `/dev/snd/controlC5`

Use the script [`usb2dev.sh`](usb2dev.sh) to map device names to its
`/dev` ports.
